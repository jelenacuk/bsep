import { Injectable, Optional } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Certificate } from '../model/new-certificate.model';
import { Certificatedto } from '../model/Certificatedto.model';
import { Constants } from './constants';
import { CertificateRevoked } from '../model/CertificateRevoked.model';

@Injectable({
  providedIn: 'root'
})
export class CertificateService {
  constructor(private http: HttpClient, private constants: Constants) { }


  createSelfSignedCertificate(dto: Certificate): Observable<boolean> {
    return this.http.post<boolean>(this.constants.path + '/api/certificate', dto);
  }


  createIssuedCertificate(dto: Certificate): Observable<boolean> {
    return this.http.post<boolean>(this.constants.path + '/api/certificate/issued', dto);
  }

  getIssuerData(id: number): Observable<any> {
    return this.http.post<any>(this.constants.path + '/api/certificate/get-issuer-data', id);
  }
  getCertificate(id: number) {
    return this.http.get<Certificatedto>(this.constants.path + '/api/certificate/get-cetrificate/' + id);
  }
  getRootCertificate() {
    return this.http.get<Certificatedto>(this.constants.path + '/api/certificate/root-certificate');
  }
  revokeCertificate(id: number): Observable<CertificateRevoked> {
    return this.http.delete<CertificateRevoked>(this.constants.path + '/api/certificate' + '/' + id);
  }


}
