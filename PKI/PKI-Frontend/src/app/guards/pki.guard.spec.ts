import { TestBed, async, inject } from '@angular/core/testing';

import { PkiGuard } from './pki.guard';

describe('PkiGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PkiGuard]
    });
  });

  it('should ...', inject([PkiGuard], (guard: PkiGuard) => {
    expect(guard).toBeTruthy();
  }));
});
