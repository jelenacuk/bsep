/* Insert Privileges */
INSERT INTO privilege (id, name) VALUES ('101', 'CREATE_KEYSTORE');
INSERT INTO privilege (id, name) VALUES ('102', 'CREATE_ROOT_CERTIFICATE');
INSERT INTO privilege (id, name) VALUES ('103', 'CREATE_INTERMIDIATE_CERTIFICATE');
INSERT INTO privilege (id, name) VALUES ('104', 'CREATE_SSL_CERTIFICATE');
INSERT INTO privilege (id, name) VALUES ('105', 'REVOKE_ROOT_CERTIFICATE');
INSERT INTO privilege (id, name) VALUES ('106', 'REVOKE_INTERMIDIATE_CERTIFICATE');
INSERT INTO privilege (id, name) VALUES ('107', 'REVOKE_SSL_CERTIFICATE');
INSERT INTO privilege (id, name) VALUES ('120', 'READ_CERTIFICATE');

/* Insert Roles */
INSERT INTO role (id, name) VALUES ('108', 'ROOT_ADMIN');
INSERT INTO role (id, name) VALUES ('109', 'ADMIN');

/* Add Privileges to Root-Admin */
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('108', '101');
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('108', '102');
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('108', '103');
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('108', '104');
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('108', '105');
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('108', '106');
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('108', '107');
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('108', '120');

/* Add Privileges to Admins */
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('109', '104');
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('109', '107');
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('109', '120');

/* Insert Admins */
INSERT INTO admin (id, password, username) VALUES (110, '$2a$10$PxvKsblywCsPRpmccc2Id.Vf5bMDvXfRMxUhmhhAL1gxXLcWDAhIa', 'root'); 
INSERT INTO admin (id, password, username) VALUES (111, '$2a$10$PxvKsblywCsPRpmccc2Id.Vf5bMDvXfRMxUhmhhAL1gxXLcWDAhIa', 'mile'); 
INSERT INTO admin (id, password, username) VALUES (112, '$2a$10$PxvKsblywCsPRpmccc2Id.Vf5bMDvXfRMxUhmhhAL1gxXLcWDAhIa', 'jelena'); 
INSERT INTO admin (id, password, username) VALUES (113, '$2a$10$PxvKsblywCsPRpmccc2Id.Vf5bMDvXfRMxUhmhhAL1gxXLcWDAhIa', 'nikola');

/* Add Roles to Admins */
INSERT INTO users_roles (user_id, role_id) VALUES (110, 108);
INSERT INTO users_roles (user_id, role_id) VALUES (111, 109);
INSERT INTO users_roles (user_id, role_id) VALUES (112, 109);
INSERT INTO users_roles (user_id, role_id) VALUES (113, 109);
 
 /* Insert Certificates */
INSERT INTO certificate (id, common_name, country ,isca, issued_cert_counter, organisation, organisation_unit, revoked, serial_number, signature_algorithm, valid_from, valid_to, issuer_id ) VALUES ('100', 'localhost', 'RS', true,  1, 'BSEP', 'PKI', false, 0, 'SHA256WithRSAEncryption', '2020-05-30 09:03:42.433000', '2030-05-30 09:03:42.433000', '100'
);
INSERT INTO certificate (id, common_name, country ,isca, issued_cert_counter, organisation, organisation_unit, revoked, serial_number, signature_algorithm, valid_from, valid_to, issuer_id ) VALUES ('101', 'localhost', 'RS', true,  1, 'BSEP', 'SIEM_CENTER1', false, 1, 'SHA256WithRSAEncryption', '2020-05-30 09:03:42.433000', '2030-05-30 09:03:42.433000', '100'
);
INSERT INTO certificate (id, common_name, country ,isca, issued_cert_counter, organisation, organisation_unit, revoked, serial_number, signature_algorithm, valid_from, valid_to, issuer_id ) VALUES ('102', 'localhost', 'RS', false,  0, 'BSEP', 'SIEM_AGENT1', false, 2, 'SHA256WithRSAEncryption', '2020-05-30 09:03:42.433000', '2030-05-30 09:03:42.433000', '101'
);