package com.siem.SIEMCenter.model;

public class SearchData {

	private String level;
	private String date;
	private String appName;
	private String service;
	private String computerName;
	private String user;
	private String system;

	public SearchData() {
	}

	public SearchData(String level, String date, String appName, String service, String computerName, String user,
			String system) {
		this.level = level;
		this.date = date;
		this.appName = appName;
		this.service = service;
		this.computerName = computerName;
		this.user = user;
		this.system = system;

	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getComputerName() {
		return computerName;
	}

	public void setComputerName(String computerName) {
		this.computerName = computerName;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

}
