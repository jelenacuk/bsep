import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { UserService } from '../service/user.service';
import { LogIn } from '../model/LogIn';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private userService: UserService,
              private snackBar: MatSnackBar, private router: Router) { }

  loginForm: FormGroup;
  hide = true;

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  get username() { return this.loginForm.controls.username.value as string; }
  get password() { return this.loginForm.controls.password.value as string; }


  onLogInSubmit() {
    const loginData = new LogIn(this.username, this.password);
    this.userService.login(loginData).subscribe(
      (response => {
        if (response != null) {
          localStorage.setItem('token', response.token);
          const jwt: JwtHelperService = new JwtHelperService();
          const info = jwt.decodeToken(response.token);
          localStorage.setItem('role', JSON.stringify(info.role));
          this.snackBar.open('Logged In successfully.');
          this.router.navigateByUrl('');
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
      }));
  }

}
