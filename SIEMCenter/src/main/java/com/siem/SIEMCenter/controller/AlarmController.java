package com.siem.SIEMCenter.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.siem.SIEMCenter.dto.AlarmDTO;
import com.siem.SIEMCenter.dto.ReportDTO;
import com.siem.SIEMCenter.dto.ReportRequestDTO;
import com.siem.SIEMCenter.model.Alarm;
import com.siem.SIEMCenter.service.AlarmService;

@RestController
@RequestMapping("api/alarms")
@CrossOrigin
public class AlarmController {
	
	private AlarmService alarmService;
	
	
	public AlarmController(AlarmService alarmService) {
		this.alarmService = alarmService;
	}
	
	@PostMapping(value = "/reports", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ReportDTO> getReport(@RequestBody ReportRequestDTO dto) {
		ReportDTO reportDTO = alarmService.report(dto);
		return new ResponseEntity<ReportDTO>(reportDTO, HttpStatus.OK);
	}
	
	@GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<AlarmDTO>> getAlarms() {
		List<Alarm> alarms = alarmService.getAlarms();
		List<AlarmDTO> dtos = alarms.stream().map(alarm->new AlarmDTO(alarm)).collect(Collectors.toList());
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

}
