export class GenerateCertificate {

    constructor(
        private commonName: string,
        private surname: string,
        private givenName: string,
        private organizationalUnit: string,
        private email: string) { }

}
