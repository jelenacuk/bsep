from random import randrange
from datetime import datetime

id = 0


# id level date time app_name service computer user system description

def generate_log(path, mode, app_name, computer_name, user, system):
    if mode == "NORMAL":
        generate_normal_log(path, app_name, computer_name, user, system)
    elif mode == "ATTACK":
        generate_attack_log(path, app_name, computer_name, user, system)


def generate_normal_log(path, app_name, computer_name, user, system):
    global id
    level = ["FATAL", "ERROR", "WARN", "INFO"]
    service = ["Loggin-Service", "Certificate-Service", "Alarm-Service", "Log-Service"]

    description = ["Someone tried to log in with wrong credentials.", "You don't have authority for certificate creation",
                   "You don't have authority to view Alarms", "You don't have authority to view logs"]
    random_num = randrange(4)
    random_num2 = randrange(4)
    description_selected = description[random_num2]
    now = datetime.today()
    now = now.strftime("%m/%d/%Y %H:%M:%S")

    log = str(id) + " " + level[
        random_num] + " " + now + " " + app_name + " " + service[
              random_num2] + " " + computer_name + " " + user + " " + system + " " + description_selected + "\n"
    id += 1
    file = open(path, "a")
    file.write(log)
    file.close()


def generate_attack_log(path, app_name, computer_name, user, system):
    global id
    level = ["FATAL", "ERROR", "WARN", "INFO"]
    service = ["Loggin-Service", "Certificate-Service", "Alarm-Service", "Log-Service"]
    now = datetime.today()
    now = now.strftime("%m/%d/%Y %H:%M:%S")
    description = ["Someone tried to log in with wrong credentials.", "You don't have authority for certificate creation.",
                   "You don't have authority to view Alarms.", "You don't have authority to view logs."]
    random_num = randrange(10)
    random_num2 = randrange(4)
    description_selected = description[random_num2]
    level_selected = ""
    if random_num <= 1:
        level_selected = "INFO"
    elif random_num <= 4:
        level_selected = "WARN"
    elif random_num < 9:
        level_selected = "ERROR"
    else:
        level_selected = "FATAL"
    file = open(path, "a")
    log = str(
        id) + " " + level_selected + " " + now + " " + app_name + " " + service[random_num2] + " " + computer_name + " " + \
          user + " " + system + " " + description_selected + "\n"

    id += 1

    file.write(log)

    file.close()
