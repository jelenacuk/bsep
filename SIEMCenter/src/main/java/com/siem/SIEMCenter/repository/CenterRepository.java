package com.siem.SIEMCenter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.siem.SIEMCenter.model.Log;

public interface CenterRepository extends MongoRepository<Log, String> {

}
