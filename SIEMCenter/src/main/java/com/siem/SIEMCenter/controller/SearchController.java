package com.siem.SIEMCenter.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.siem.SIEMCenter.dto.LogOutputDTO;
import com.siem.SIEMCenter.model.Log;
import com.siem.SIEMCenter.model.SearchData;
import com.siem.SIEMCenter.service.CenterService;

@RestController
@RequestMapping(value = "api/search")
@CrossOrigin
public class SearchController {

	private CenterService centerService;

	public SearchController(CenterService centerService) {
		super();
		this.centerService = centerService;
	}

	@PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<LogOutputDTO>> searchLogs(@RequestBody SearchData data) {
		List<Log> logs = centerService.search(data);
		List<LogOutputDTO> outputDTOs = logs.stream().map(log -> new LogOutputDTO(log)).collect(Collectors.toList());
		return new ResponseEntity<List<LogOutputDTO>>(outputDTOs, HttpStatus.OK);
	}

	@PostMapping(value = "regex", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<LogOutputDTO>> searchLogsRegex(@RequestBody SearchData data) {
		List<Log> logs = centerService.searchReg(data);
		List<LogOutputDTO> outputDTOs = logs.stream().map(log -> new LogOutputDTO(log)).collect(Collectors.toList());
		return new ResponseEntity<List<LogOutputDTO>>(outputDTOs, HttpStatus.OK);
	}

}
