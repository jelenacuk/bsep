import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { RequestReportDto } from '../model/RequestReportDto';
import { ReportService } from '../service/reports.service';
import { ReportDto } from '../model/ReportDto';
import { NumberValueAccessor } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  startDate: Date;
  endDate: Date;
  report: ReportDto;
  // select
  typeOfDate = 'recorded';
  showLogs = false;
  showAlarms = false;
  // chart
  barChartOptions: ChartOptions = { responsive: true };
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];
  barChartLabelsLogs: Label[] = ['1', '2', '3', '4', '5'];
  barChartDataLogs: ChartDataSets[] = [{ data: [10, 9, 8, 7, 6], label: 'Num of Logs' }];
  barChartLabelsAlarms: Label[] = ['1', '2', '3', '4', '5'];
  barChartDataAlarms: ChartDataSets[] = [{ data: [10, 9, 8, 7, 6], label: 'Num of Alarms' }];

  constructor(private reportService: ReportService, private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  applyForLogs(parameter: string) {

    if (!this.startDate) {
      this.snackBar.open('Start date must be  defined!');
      return;
    }
    if (!this.endDate) {
      this.endDate = new Date();
    }
    if (this.startDate > this.endDate) {
      this.snackBar.open('Start date must be before end date.');
    }
    const reportDto: RequestReportDto = new RequestReportDto(this.startDate, this.endDate, parameter, this.typeOfDate);
    this.reportService.getReportForLogs(reportDto).subscribe(
      (response => {
        if (response !== null) {
          console.log(JSON.stringify(response));
          this.report = response;
          this.barChartLabelsLogs = this.report.labels;
          this.barChartDataLogs = [{ data: this.report.data, label: 'Num of Logs' }];
          this.showLogs = true;
        }
      }),
      (error => {
        this.snackBar.open('You do not have authority to see reports.');
      })
    );

  }

  applyForAlarms(parameter: string) {

    if (this.startDate === undefined) {
      this.snackBar.open('Start date must be  defined!');
      return;
    }
    if (this.endDate === undefined) {
      this.endDate = new Date();
    }
    if (this.startDate > this.endDate) {
      this.snackBar.open('Start date must be before end date.');
    }
    const reportDto: RequestReportDto = new RequestReportDto(this.startDate, this.endDate, parameter, '');
    this.reportService.getReportForAlarms(reportDto).subscribe(
      (response => {
        if (response !== null) {
          console.log(JSON.stringify(response));
          this.report = response;
          this.barChartLabelsAlarms = this.report.labels;
          this.barChartDataAlarms = [{ data: this.report.data, label: 'Num of Alarms' }];
          this.showAlarms = true;
        }
      }),
      (error => {
        this.snackBar.open('You do not have authority to see reports.');
      })
    );
  }

}
