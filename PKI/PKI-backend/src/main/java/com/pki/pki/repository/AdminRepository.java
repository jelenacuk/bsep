package com.pki.pki.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pki.pki.model.Admin;

public interface AdminRepository extends JpaRepository<Admin, Long> {

	Optional<Admin> findByUsername(String username);

}
