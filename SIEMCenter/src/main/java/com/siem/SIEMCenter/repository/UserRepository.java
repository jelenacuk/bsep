package com.siem.SIEMCenter.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.siem.SIEMCenter.model.User;

public interface UserRepository extends JpaRepository<User, Long>{
	Optional<User> findByUsername(String name);
}
