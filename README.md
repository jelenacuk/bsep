Instrukcije za pokretanje PKI

Potrebno je da se pokrene mysql baza podataka na portu 3306
username:root
password:root
Username i password ce biti promenjeni u kasnijim fazama zbog sigurnosti. Trenutno su ove vrednosti zbog jednostavnosti.

Potom je potrebno pokrenuti Spring server (Spring aplikacija se nalazi u PKI/PKI-backend)
Spring server se pokrece na portu 8443

Angular aplikacija se nalazi u (PKI\PKI-Frontend)
Angular server se pokrece sa ng serve --ssl true

Prilikom prvog koriscenja angular aplikacije potrebno je pokrenuti komandu "npm install"
Angular server se pokrece na https://localhost:4200
Ukoliko browser izbaci da konekcija nije privatna potrebno je ici na advanced i Proceed to localhost (unsafe)
Takodje je potebno u search bar google chrome-a uneti komandu "chrome://flags/#allow-insecure-localhost" i dozvoliti "Allow invalid certificates for resources loaded from localhost"
Ako korisnik nije ulogovan bice redirektovan na https://localhost:4200 gde je potrebno da se uloguje
Username:Admin
Password:Admin
I ovo ce biti izmenjeno sa bezbednijim korisnickim imenom i passwordom u kasnijim verzijama 

Nakon logovanja korisnik je redirektovan na https://localhost:4200/home
gde ako nema ni jedan sertifikat bice prikazano samo dugme za kreiranje sertifikata
Ako postoji root sertifikat, bice prikazani njegovi detalji, i opcija da kreira pod sertifikat ili da povuce sertifikat.
Takodje, ako je taj sertifikat koriscen za izdavanje pod sertifikata, njemu je prikazana lista svih njegovih pod sertifikata

Pritiskom da dugme za kreiranje novog sertifikata otvara se forma za kreiranje novog sertifikata. Nakon sto korisnik unese podatke o sertifikatu, sertifikat je kreiran
Pritiskom na dugme revoke, sertifikat je povucen tj, flag revoked se postavlja na: true

Pokretanje SIEM Centra

Potrebno je pokrenuti mysql bazu, na gore naveden nacin i MongoDB bazu za skladistenje logova.
mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=false 

SIEM Centar ce biti podignut na portu 8444

SIEM Centar Front aplikacija se podize sa ng serve --ssl true iz njenog root foldera.
Bice podignuta na portu https://localhost:4201

Potrebno je pokrenuti i SIEM Agent aplikaciju na portu 8445 nakon pokretanja SIEM Centra.
SIEM Agent cita Windows sistem logove i logove koje SIMULATOR aplikacija generise.

Na kraju treba pokrenuti SIMULATOR python aplikaciju koja je zaduzena za generisanje logova.

