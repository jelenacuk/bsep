package com.siem.SIEMCenter.dto;

public class RegexDTO {
	private String text;

	public RegexDTO() {
		super();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
}
