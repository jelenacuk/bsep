package com.pki.pki.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.NumberFormat;

import com.sun.istack.NotNull;

public class NewCertificateDTO {

	@NotNull
	@Size(min = 3)
	@Pattern(regexp = "^[A-Za-z][A-Za-z0-9\\'\\-\\.]+([\\ A-Za-z][A-Za-z0-9\\'\\-\\.]+)*")
	private String commonName;
	@NotNull
	@Size(min = 2)
	@Pattern(regexp = "^[A-Za-z][A-Za-z\\'\\-]+([\\ A-Za-z][A-Za-z\\'\\-]+)*")
	private String givenName;
	@NotNull
	@Size(min = 2)
	@Pattern(regexp = "^[A-Za-z][A-Za-z\\'\\-]+([\\ A-Za-z][A-Za-z\\'\\-]+)*")
	private String surname;
	@NotNull
	@Pattern(regexp = "^[A-Za-z][A-Za-z0-9\\'\\-]+([\\ A-Za-z][A-Za-z0-9\\'\\-]+)*")
	private String organizationalUnit;
	@NotNull
	@Pattern(regexp = "^[A-Za-z][A-Za-z0-9\\'\\-]+([\\ A-Za-z][A-Za-z0-9\\'\\-]+)*")
	private String organization;
	@NotNull
	@Pattern(regexp = "^[A-Za-z][A-Za-z\\'\\-]+([\\ A-Za-z][A-Za-z\\'\\-]+)*")
	private String countryName;
	@NotNull
	@Email
	private String email;
	@NotNull
	@Pattern(regexp = "^[A-Za-z][A-Za-z0-9\\'\\-\\.]+([\\ A-Za-z][A-Za-z0-9\\'\\-\\.]+)*")
	private Long issuerId;
	@NotNull
	private boolean ca;
	@NotNull
	@NumberFormat
	private int duration;
	
	public NewCertificateDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public NewCertificateDTO(Long issuerId, String organisation, String country, CertificateRequestDTO dto) {
		// TODO Auto-generated constructor stub
		this.issuerId = issuerId;
		this.commonName = dto.getCommonName();
		this.givenName = dto.getGivenName();
		this.surname = dto.getSurname();
		this.organizationalUnit = dto.getOrganizationalUnit();
		this.organization = organisation;
		this.countryName = country;
		this.email = dto.getEmail();
		this.ca = false;
		this.duration = 3;
		
	}

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public String getOrganizationalUnit() {
		return organizationalUnit;
	}

	public void setOrganizationalUnit(String organizationalUnit) {
		this.organizationalUnit = organizationalUnit;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(Long issuerAlias) {
		this.issuerId = issuerAlias;
	}

	public boolean isCA() {
		return ca;
	}

	public void setCA(boolean isCA) {
		this.ca = isCA;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	
}
