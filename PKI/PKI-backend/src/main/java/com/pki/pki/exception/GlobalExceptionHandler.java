package com.pki.pki.exception;

import java.util.Date;

import javax.persistence.EntityNotFoundException;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.pki.pki.dto.ErrorDetailsDTO;
import com.pki.pki.utilities.DateUtilities;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(value = { EntityNotFoundException.class })
	public ResponseEntity<ErrorDetailsDTO> handleNotFoundExceptions(Exception exception) {
		return genericHandler(exception, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = { ConstraintViolationException.class })
	public ResponseEntity<ErrorDetailsDTO> handleConflictExceptions(Exception exception) {
		return genericHandler(exception, HttpStatus.CONFLICT);
	}

	@ExceptionHandler(value = { Exception.class })
	public ResponseEntity<ErrorDetailsDTO> handleExceptions(Exception exception) {
		return genericHandler(exception, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private ResponseEntity<ErrorDetailsDTO> genericHandler(Exception exception, HttpStatus httpStatus) {
		exception.printStackTrace();
		return new ResponseEntity<>(
				new ErrorDetailsDTO(exception.getMessage(), DateUtilities.convertToString(new Date()), httpStatus),
				httpStatus);
	}

}
