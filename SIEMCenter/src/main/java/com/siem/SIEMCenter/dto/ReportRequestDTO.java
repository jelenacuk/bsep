package com.siem.SIEMCenter.dto;

import java.util.Date;

public class ReportRequestDTO {

	private Date startDate;
	private Date endDate;
	private String parameter;
	private String typeOfDate;
	
	public ReportRequestDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getTypeOfDate() {
		return typeOfDate;
	}

	public void setTypeOfDate(String typeOfDate) {
		this.typeOfDate = typeOfDate;
	}
}
