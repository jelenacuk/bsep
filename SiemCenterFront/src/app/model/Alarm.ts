export class Alarm {
    public id: string;
    public message: string;
    public appName: string;
    public service: string;
    public computerName: string;
    public user: string;
    public creationDate: Date;
}