package com.pki.pki.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.pki.pki.service.JwtUserDetailsService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private JwtAuthenticationEntryPoint restAuthenticationEntryPoint;
	private JwtRequestFilter jwtRequestFilter;
	private JwtUserDetailsService jwtUserDetailsService;

	public WebSecurityConfig(JwtAuthenticationEntryPoint restAuthenticationEntryPoint,
			JwtRequestFilter jwtRequestFilter, JwtUserDetailsService jwtUserDetailsService) {
		this.restAuthenticationEntryPoint = restAuthenticationEntryPoint;
		this.jwtRequestFilter = jwtRequestFilter;
		this.jwtUserDetailsService = jwtUserDetailsService;
	}
	
	@Bean
	public GrantedAuthorityDefaults grantedAuthorityDefaults() {
	    return  new GrantedAuthorityDefaults("");
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				// comunication between client and server is stateless
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()

				// for unauthorized request send 401 error
				.exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint).and()

				// don't authenticate this particular request
				.authorizeRequests().antMatchers("/auth/**").permitAll()
				.antMatchers(HttpMethod.OPTIONS, "/api/**").permitAll()
				.antMatchers(HttpMethod.GET, "/api/certificate/get-cetrificate/**").hasAuthority("READ_CERTIFICATE")
				.antMatchers(HttpMethod.GET, "/api/certificate/root-certificate/**").hasAuthority("READ_CERTIFICATE")
				.antMatchers(HttpMethod.DELETE, "/api/certificate/**").hasAuthority("REVOKE_ROOT_CERTIFICATE")
				.antMatchers(HttpMethod.POST, "/api/certificate").hasAuthority("CREATE_ROOT_CERTIFICATE")
				.antMatchers(HttpMethod.POST, "/api/certificate/issued").hasAuthority("CREATE_SSL_CERTIFICATE")
				.antMatchers(HttpMethod.POST, "/api/certificate/get-issuer-data").hasAuthority("READ_CERTIFICATE")
				.antMatchers(HttpMethod.GET, "/api/key-store").hasAuthority("CREATE_KEYSTORE")
				.antMatchers(HttpMethod.POST, "/api/certificate/request").permitAll()

				
				// all other requests need to be authenticated
				.anyRequest().authenticated().and();

		// intercept every request and add filter
		http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

		http.csrf().disable();
		http.requiresChannel().anyRequest().requiresSecure();
	}

}
