package com.siem.SIEMCenter.dto;

import org.springframework.http.HttpStatus;

public class ErrorDetailsDTO {
	private String message;
	private String timestamp;
	private Integer status;
	private String error;

	public ErrorDetailsDTO() {
	}

	public ErrorDetailsDTO(String message, String timestamp, HttpStatus httpStatus) {
		this.message = message;
		this.timestamp = timestamp;
		this.status = httpStatus.value();
		this.error = httpStatus.name();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
