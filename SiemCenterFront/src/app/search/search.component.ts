import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Log } from '../model/Log';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SearchDto } from '../model/SearchDto';
import { SearchService } from '../service/search.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  providers: [DatePipe]
})
export class SearchComponent implements OnInit {
  dialog: boolean;
  foundLogs: Array<Log>;
  searchForm: FormGroup;
  keywords: Array<string> = new Array<string>();
  selectFormControl = new FormControl('', Validators.required);

  @Output() searchResult = new EventEmitter<Array<Log>>();
  constructor(private searchService: SearchService, private snackBar: MatSnackBar, private formBuilder: FormBuilder,
    private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.buildForm();

  }

  openDialogue() {
    this.buildForm();
    this.dialog = true;
  }

  closeQuestionDialog() {
  }

  search() {
    const dto: SearchDto = this.getSearchData();
    this.searchService.search(dto).subscribe(
      (response => {
        if (response !== null) {
          this.foundLogs = response;
          console.log(response);
          this.searchResult.emit(response);
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
      })
    );
  }
  buildForm() {
    this.searchForm = this.formBuilder.group({
      level: ['', []],
      appName: ['', []],
      date: ['', []],
      service: ['', []],
      computerName: ['', []],
      regex: ['', []],
      system: ['', []],
      user: ['', []]
    });
  }

  getSearchData(): SearchDto {
    const dto: SearchDto = new SearchDto();
    dto.level = this.searchForm.controls.level.value;
    dto.system = this.searchForm.controls.system.value;
    dto.appName = this.searchForm.controls.appName.value;
    if (dto.date !== '') {
      dto.date = this.datePipe.transform(dto.date, 'dd-MM-yyyy');
    }
    dto.service = this.searchForm.controls.service.value;
    dto.computerName = this.searchForm.controls.computerName.value;
    dto.user = this.searchForm.controls.user.value;
    return dto;
  }

  searchRegex() {
    const regex: SearchDto = this.getSearchData();

    this.searchService.searchRegex(regex).subscribe(
      (response => {
        if (response !== null) {
          this.foundLogs = response;
          console.log(response);
          this.searchResult.emit(response);
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
      })
    );
  }

  clear() {
    this.searchForm = this.formBuilder.group({
      level: ['', []],
      appName: ['', []],
      date: ['', []],
      service: ['', []],
      computerName: ['', []],
      regex: ['', []],
      system: ['', []],
      user: ['', []]
    });
  }

}
