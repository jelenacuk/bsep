package com.siem.SIEMCenter.service;

import javax.annotation.PreDestroy;

import org.kie.api.KieBase;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.KieServices;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.stereotype.Service;

@Service
public class KnowledgeService {

	private final KieContainer kieContainer;
    private KieSession kieSession;
    private AlarmService alarmService;

    public KnowledgeService(KieContainer kieContainer, AlarmService alarmService) {
        this.kieContainer = kieContainer;
        this.alarmService = alarmService;
    }

    @PreDestroy
    private void release(){
        this.kieSession.dispose();
    }

    public KieContainer getKieContainer() {
        return kieContainer;
    }

    public KieSession getKieSession() {
        if(kieSession == null){
        	KieServices ks = KieServices.Factory.get();
    		KieBaseConfiguration kbconf = ks.newKieBaseConfiguration();
    		kbconf.setOption(EventProcessingOption.STREAM);
    		KieBase kbase = getKieContainer().newKieBase(kbconf);
    		kieSession = kbase.newKieSession();
    		kieSession.setGlobal("alarmService", alarmService);
        }
        return kieSession;
    }

    public void setKieSession(KieSession kieSession) {
        this.kieSession = kieSession;
    }
}
