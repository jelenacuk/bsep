import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RequestReportDto } from '../model/RequestReportDto';
import { ReportDto } from '../model/ReportDto';
import { Alarm } from '../model/Alarm';

@Injectable({
    providedIn: 'root'
})
export class ReportService {

    constructor(private http: HttpClient) { }
    path = 'https://localhost:8444';

    getReportForLogs(reportDto: RequestReportDto): Observable<ReportDto> {
        return this.http.post<ReportDto>(this.path + '/api/center/reports', reportDto);
    }

    getReportForAlarms(reportDto: RequestReportDto): Observable<ReportDto> {
        return this.http.post<ReportDto>(this.path + '/api/alarms/reports', reportDto);
    }

    getAlarms(): Observable<Array<Alarm>> {
        return this.http.get<Array<Alarm>>(this.path + '/api/alarms');
    }
}
