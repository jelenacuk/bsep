package com.siem.SIEMCenter.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.siem.SIEMCenter.dto.CertificateRequestDTO;
import com.siem.SIEMCenter.dto.LogDTO;
import com.siem.SIEMCenter.dto.ReportDTO;
import com.siem.SIEMCenter.dto.ReportRequestDTO;
import com.siem.SIEMCenter.service.CenterService;

@RestController
@RequestMapping("api/center")
@CrossOrigin
public class CenterController {
	private CenterService centerService;

	public CenterController(CenterService centerService) {
		this.centerService = centerService;
	}

	@PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String saveLog(@RequestBody LogDTO dto) {
		centerService.saveLog(dto);
		return "OK";
	}
	
	
	@PostMapping(value = "/reports", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ReportDTO> getReport(@RequestBody ReportRequestDTO dto) {
		ReportDTO reportDTO = centerService.report(dto);
		return new ResponseEntity<ReportDTO>(reportDTO, HttpStatus.OK);
	}
	@PostMapping(value="/request-certificate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> requestCertificate(@RequestBody CertificateRequestDTO dto) throws Exception {
		centerService.requestCertificate(dto);
		return new ResponseEntity<Boolean>(true,HttpStatus.OK);
	}
}
