package com.siem.SIEMCenter.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import org.kie.api.definition.type.Expires;
import org.kie.api.definition.type.Role;

@Role(Role.Type.EVENT)
@Expires("1m")
@Document
public class Log {
	@Id
	private String id;
	private String level;
	private Date dateRecorded;
	private Date dateProcessed;
	private String appName;
	private String service;
	private String computerName;
	private String user;
	private String system;
	private String description;

	public Log() {
	}

	public Log(String id, String level, Date dateRecorded, Date dateProcessed, String appName, String service,
			String computerName, String user, String system, String description) {
		this.id = id;
		this.level = level;
		this.dateRecorded = dateRecorded;
		this.dateProcessed = dateProcessed;
		this.appName = appName;
		this.service = service;
		this.computerName = computerName;
		this.user = user;
		this.system = system;
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Date getDateRecorded() {
		return dateRecorded;
	}

	public void setDateRecorded(Date dateRecorded) {
		this.dateRecorded = dateRecorded;
	}

	public Date getDateProcessed() {
		return dateProcessed;
	}

	public void setDateProcessed(Date dateProcessed) {
		this.dateProcessed = dateProcessed;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getComputerName() {
		return computerName;
	}

	public void setComputerName(String computerName) {
		this.computerName = computerName;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDescription() {
		return description;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
