package com.pki.pki.service;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.pki.pki.config.JwtTokenUtil;
import com.pki.pki.dto.LoginDTO;

@Service
public class AuthenticationService {

	private AuthenticationManager authenticationManager;
	private JwtUserDetailsService jwtUserDetailsService;
	private JwtTokenUtil jwtTokenUtil;
	

	public AuthenticationService(AuthenticationManager authenticationManager,
			JwtUserDetailsService jwtUserDetailsService, JwtTokenUtil jwtTokenUtil) {
		super();
		this.authenticationManager = authenticationManager;
		this.jwtUserDetailsService = jwtUserDetailsService;
		this.jwtTokenUtil = jwtTokenUtil;
	}

	public String logIn(LoginDTO authenticationRequest) {
		System.out.println(authenticationRequest.getUsername());
		System.out.println(authenticationRequest.getPassword());
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					authenticationRequest.getUsername(), authenticationRequest.getPassword()));
		} catch (BadCredentialsException | InternalAuthenticationServiceException e) {
			throw new BadCredentialsException(authenticationRequest.getUsername());
		}
		final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());
		return jwtTokenUtil.generateToken(userDetails);
	}

}
