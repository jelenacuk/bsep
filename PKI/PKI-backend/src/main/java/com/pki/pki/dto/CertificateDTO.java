package com.pki.pki.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;

import com.pki.pki.model.Certificate;

public class CertificateDTO {

	private Long id;
	private String commonName;
	private String serialNumber;
	private String signatureAlgorithm;
	private Long issuerId;
	private boolean isCA;
	private Date validFrom;
	private Date validTo;
	private List<CertificateDTO> childCertificates;
	private Boolean revoked;
	private String issuerName;

	private String organisation;
	private String organisationUnit;
	private String country;

	public CertificateDTO(Long id, String commonName, String serialNumber, String signatureAlgorithm, Long issuerId,
			boolean isCA, Date validFrom, Date validTo, List<CertificateDTO> childCertificates, Boolean revoked) {
		this.id = id;
		this.commonName = commonName;
		this.serialNumber = serialNumber;
		this.signatureAlgorithm = signatureAlgorithm;
		this.issuerId = issuerId;
		this.isCA = isCA;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.childCertificates = childCertificates;
		this.revoked = revoked;
	}

	public CertificateDTO(Certificate certificate) {
		this.id = certificate.getId();
		this.commonName = certificate.getCommonName();
		this.serialNumber = certificate.getSerialNumber();
		this.signatureAlgorithm = certificate.getSignatureAlgorithm();
		this.issuerId = certificate.getIssuer().getId();
		this.isCA = certificate.isCA();
		this.validFrom = certificate.getValidFrom();
		this.validTo = certificate.getValidTo();
		this.revoked = certificate.getRevoked();
		this.organisation = certificate.getOrganisation();
		this.organisationUnit = certificate.getOrganisationUnit();
		this.country = certificate.getCountry();
		this.issuerName = certificate.getCountry() + " " + certificate.getOrganisation() + " "
				+ certificate.getOrganisationUnit();

	}

	public CertificateDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getSignatureAlgorithm() {
		return signatureAlgorithm;
	}

	public void setSignatureAlgorithm(String signatureAlgorithm) {
		this.signatureAlgorithm = signatureAlgorithm;
	}

	public Long getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(Long issuerId) {
		this.issuerId = issuerId;
	}

	public boolean isCA() {
		return isCA;
	}

	public void setCA(boolean isCA) {
		this.isCA = isCA;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public List<CertificateDTO> getChildCertificates() {
		return childCertificates;
	}

	public void setChildCertificates(List<CertificateDTO> childCertificates) {
		this.childCertificates = childCertificates;
	}

	public Boolean getRevoked() {
		return revoked;
	}

	public void setRevoked(Boolean revoked) {
		this.revoked = revoked;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getOrganisationUnit() {
		return organisationUnit;
	}

	public void setOrganisationUnit(String organisationUnit) {
		this.organisationUnit = organisationUnit;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

}
