package com.siem.SIEMCenter.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.siem.SIEMCenter.model.Privilege;

public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {
	Optional<Privilege> findByName(String name);
}
