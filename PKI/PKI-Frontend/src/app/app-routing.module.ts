import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateCertificateComponent } from './create-certificate/create-certificate.component';
import { LoginComponent } from './login/login.component';
import { CertificateDetailsComponent } from './certificate-details/certificate-details.component';
import { PkiGuard } from './guards/pki.guard';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  { path: 'create-certificate', component: CreateCertificateComponent, canActivate: [PkiGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: CertificateDetailsComponent, canActivate: [PkiGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
