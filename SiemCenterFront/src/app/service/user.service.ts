import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LogIn } from '../model/LogIn';
import { Observable } from 'rxjs';
import { JwtResponse } from '../model/JwtResponse';
import { GenerateCertificate } from '../model/GenerateCertificate';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  private path = 'https://localhost:8444';

  login(dto: LogIn): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.path + '/auth/login', dto);
  }

  generateCertificate(generateCertificateDto: GenerateCertificate): Observable<boolean> {
    return this.http.post<boolean>(this.path + '/api/center/request-certificate', generateCertificateDto);
}
}
