import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ReportsComponent } from './reports/reports.component';
import { AlarmComponent } from './alarm/alarm.component';
import { LoginComponent } from './login/login.component';
import { GenerateCertificateComponent } from './generate-certificate/generate-certificate.component';
import { HomeGuard } from './guards/home.guard';
import { ReportsGuard } from './guards/reports.guard';
import { AlarmsGuard } from './guards/alarms.guard';
import { RequestCertificateGuard } from './guards/request-certificate.guard';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [HomeGuard] },
  { path: 'reports', component: ReportsComponent, canActivate: [HomeGuard, ReportsGuard] },
  { path: 'alarms', component: AlarmComponent, canActivate: [HomeGuard, AlarmsGuard] },
  { path: 'generate-certificate', component: GenerateCertificateComponent, canActivate: [HomeGuard, RequestCertificateGuard] },
  { path: 'login', component: LoginComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

