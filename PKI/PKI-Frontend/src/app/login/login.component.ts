import { Component, OnInit } from '@angular/core';
import { LogIn } from '../model/logIn';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LoginService } from '../service/login.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  hide = true;
  token: string;
  loginForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private loginService: LoginService, private snackBar: MatSnackBar,
    private router: Router) { }
  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  get username() { return this.loginForm.controls.username.value as string; }
  get password() { return this.loginForm.controls.password.value as string; }




  onLogInSubmit() {
    if (this.username && this.password) {
      const loginData = new LogIn(this.username, this
        .password);
      this.loginService.login(loginData).subscribe(
        (response => {
          if (response != null) {
            console.log(localStorage.getItem('token'));
            localStorage.setItem('token', response.token);
            const jwt: JwtHelperService = new JwtHelperService();
            const info = jwt.decodeToken(response.token);
            console.log(info);
            localStorage.setItem('role', JSON.stringify(info.role));
            this.snackBar.open('Logged In successfully.');
            this.router.navigateByUrl('');
          }
        }),
        (error => {
          this.snackBar.open(error.error.message);
        }));
    }
  }


}
