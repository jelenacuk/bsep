package com.siem.SIEMCenter.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.collections4.map.HashedMap;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.siem.SIEMCenter.dto.CertificateRequestDTO;
import com.siem.SIEMCenter.dto.LogDTO;
import com.siem.SIEMCenter.dto.ReportDTO;
import com.siem.SIEMCenter.dto.ReportRequestDTO;
import com.siem.SIEMCenter.model.Log;
import com.siem.SIEMCenter.model.SearchData;
import com.siem.SIEMCenter.repository.CenterRepository;

@Service
public class CenterService {

	private CenterRepository repository;
	private KnowledgeService knowladgeService;
	private MongoTemplate mongoTemplate;

	private final String pkiPath = "https://localhost:8443/api/certificate/request";
	RestTemplate restTemplate;
	@Value("${server.ssl.key-alias}")
	private String alias;

	@Value("${server.ssl.key-store-password}")
	private String password;

	public CenterService(CenterRepository repository, KnowledgeService knowladgeService, MongoTemplate mongoTemplate,
			RestTemplate restTemplate) {
		this.repository = repository;
		this.knowladgeService = knowladgeService;
		this.mongoTemplate = mongoTemplate;
		this.restTemplate = restTemplate;

	}

	public void saveLog(LogDTO dto) {
		KieSession kieSession = knowladgeService.getKieSession();
		String[] separated = dto.getText().split(" ");
		System.out.println(dto.getText());
		if (separated.length >= 10) {
			try {
				StringBuilder descriptionBuilder = new StringBuilder();
				for (int i = 9; i < separated.length; i++) {
					descriptionBuilder.append(separated[i]);
					descriptionBuilder.append(" ");
				}

				// id level date time app_name service computer user system description
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

				Log l = new Log(separated[0], separated[1], sdf.parse(separated[2] + " " + separated[3]), new Date(),
						separated[4], separated[5], separated[6], separated[7], separated[8],
						descriptionBuilder.toString());
				repository.save(l);
				kieSession.insert(l);

			} catch (Exception e) {
				Log l = new Log();
				l.setDateProcessed(new Date());
				l.setDescription(dto.getText());
				repository.save(l);
				kieSession.insert(l);

			}
		} else {
			Log l = new Log();
			l.setDateProcessed(new Date());
			l.setDescription(dto.getText());
			repository.save(l);
			kieSession.insert(l);
		}

		kieSession.fireAllRules();
	}

	public List<Log> search(SearchData searchData) {
		Query query = new Query();

		if (!searchData.getLevel().equals("")) {
			query.addCriteria(Criteria.where("level").is(searchData.getLevel()));
		}
		if (!searchData.getAppName().equals("")) {
			query.addCriteria(Criteria.where("appName").is(searchData.getAppName()));
		}
		if (!searchData.getSystem().equals("")) {
			query.addCriteria(Criteria.where("system").is(searchData.getSystem()));
		}
		if (!searchData.getService().equals("")) {
			query.addCriteria(Criteria.where("service").is(searchData.getService()));
		}
		if (!searchData.getComputerName().equals("")) {
			query.addCriteria(Criteria.where("computerName").is(searchData.getComputerName()));
		}
		if (!searchData.getUser().equals("")) {
			query.addCriteria(Criteria.where("user").is(searchData.getUser()));
		}

		List<Log> filtered = mongoTemplate.find(query, Log.class);

		return filtered;
	}

	public List<Log> searchReg(SearchData searchData) {
		System.out.println(searchData.getAppName());
		Query query = new Query();

		if (!searchData.getLevel().equals("")) {
			query.addCriteria(Criteria.where("level").regex(searchData.getLevel()));
		}
		if (!searchData.getAppName().equals("")) {
			query.addCriteria(Criteria.where("appName").regex(searchData.getAppName()));
		}
		if (!searchData.getSystem().equals("")) {
			query.addCriteria(Criteria.where("system").regex(searchData.getSystem()));
		}
		if (!searchData.getService().equals("")) {
			query.addCriteria(Criteria.where("service").regex(searchData.getService()));
		}
		if (!searchData.getComputerName().equals("")) {
			query.addCriteria(Criteria.where("computerName").regex(searchData.getComputerName()));
		}
		if (!searchData.getUser().equals("")) {
			query.addCriteria(Criteria.where("user").regex(searchData.getUser()));
		}
		List<Log> filtered = mongoTemplate.find(query, Log.class);

		return filtered;
	}

	public ReportDTO report(ReportRequestDTO dto) {
		List<Log> logs = repository.findAll();
		List<Log> filteredLogs = null;
		if (dto.getTypeOfDate().equals("recorded")) {
			filteredLogs = logs.stream().filter(l -> l.getDateRecorded() != null
					&& l.getDateRecorded().after(dto.getStartDate()) && l.getDateRecorded().before(dto.getEndDate()))
					.collect(Collectors.toList());
		} else {
			filteredLogs = logs.stream().filter(l -> l.getDateProcessed() != null
					&& l.getDateProcessed().after(dto.getStartDate()) && l.getDateProcessed().before(dto.getEndDate()))
					.collect(Collectors.toList());
		}
		ReportDTO report = null;
		if (dto.getParameter().contentEquals("computerName")) {
			report = reportByComputerName(filteredLogs);
		} else if (dto.getParameter().contentEquals("level")) {
			report = reportByLevel(filteredLogs);
		} else {
			report = reportBySystem(filteredLogs);
		}
		return report;
	}

	private ReportDTO reportByComputerName(List<Log> filteredLogs) {
		Map<String, Integer> report_map = new HashedMap<String, Integer>();
		for (Log log : filteredLogs) {
			if (report_map.containsKey(log.getComputerName())) {
				int numOfLogs = report_map.get(log.getComputerName());
				report_map.put(log.getComputerName(), numOfLogs + 1);
			} else {
				report_map.put(log.getComputerName(), 1);
			}
		}
		return extractDataFromMap(report_map);
	}

	private ReportDTO reportByLevel(List<Log> filteredLogs) {
		Map<String, Integer> report_map = new HashedMap<String, Integer>();
		for (Log log : filteredLogs) {
			if (report_map.containsKey(log.getLevel())) {
				int numOfLogs = report_map.get(log.getLevel());
				report_map.put(log.getLevel(), numOfLogs + 1);
			} else {
				report_map.put(log.getLevel(), 1);
			}
		}
		return extractDataFromMap(report_map);
	}

	private ReportDTO reportBySystem(List<Log> filteredLogs) {
		Map<String, Integer> report_map = new HashedMap<String, Integer>();
		for (Log log : filteredLogs) {
			if (report_map.containsKey(log.getSystem())) {
				int numOfLogs = report_map.get(log.getSystem());
				report_map.put(log.getSystem(), numOfLogs + 1);
			} else {
				report_map.put(log.getSystem(), 1);
			}
		}
		return extractDataFromMap(report_map);
	}

	private ReportDTO extractDataFromMap(Map<String, Integer> report_map) {
		List<String> labels = new ArrayList<String>();
		List<Integer> data = new ArrayList<Integer>();
		for (Map.Entry<String, Integer> entry : report_map.entrySet()) {
			labels.add(entry.getKey());
			data.add(entry.getValue());
		}
		return new ReportDTO(labels, data);
	}

	public void requestCertificate(CertificateRequestDTO dto) throws Exception {
		dto.setAlias(alias);

		String issuerFilename = "./src/main/resources/PKI_to_"+ alias +".jks";
		KeyStore keyStore = KeyStore.getInstance("JKS", "SUN");
		keyStore.load(new FileInputStream(issuerFilename), password.toCharArray());
		final Key key = (PrivateKey) keyStore.getKey(alias, password.toCharArray());
		
		Cipher encrypt = Cipher.getInstance("RSA");
		encrypt.init(Cipher.ENCRYPT_MODE, key);
		byte[] encryptedMessage = encrypt.doFinal(dto.getAlias().getBytes());
		dto.setEncryptedAlias(Base64.getEncoder().encodeToString(encryptedMessage));

		ResponseEntity<String> encodedFileRE = restTemplate.postForEntity(pkiPath, dto, String.class);
	
		byte[] fileContend = Base64.getDecoder().decode(encodedFileRE.getBody());
		String filename = alias + "_to_" + dto.getOrganizationalUnit() + ".jks";
		try (FileOutputStream fos = new FileOutputStream("./src/main/resources/"+filename)) {
			   fos.write(fileContend);
			}
	}
}
