package com.pki.pki.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pki.pki.model.Certificate;

public interface CertificateRepository extends JpaRepository<Certificate, Long> {

	@Query("SELECT c from Certificate c join c.issuer i where c.commonName = i.commonName")
	public List<Certificate> findRoots();

	public List<Certificate> findByIssuer(Certificate fatherCertificate);

	public Certificate findOneByCommonName(String commonName);
	
	public Certificate findOneByOrganisationUnit(String ou);

	public List<Certificate> findByRevokedTrue();
}
