import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { UserService } from '../service/user.service';
import { GenerateCertificate } from '../model/GenerateCertificate';

@Component({
  selector: 'app-generate-certificate',
  templateUrl: './generate-certificate.component.html',
  styleUrls: ['./generate-certificate.component.scss']
})
export class GenerateCertificateComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private userService: UserService,
    private snackBar: MatSnackBar, private router: Router) { }

  generateCertificateForm: FormGroup;
  hide = true;

  ngOnInit(): void {
    this.generateCertificateForm = this.formBuilder.group({
      commonName: ['', [Validators.required]],
      surname: ['', [Validators.required]],
      givenName: ['', [Validators.required]],
      organizationalUnit: ['', [Validators.required]],
      email: ['', [Validators.required]],
    });
  }

  get commonName() { return this.generateCertificateForm.controls.commonName.value as string; }
  get surname() { return this.generateCertificateForm.controls.surname.value as string; }
  get givenName() { return this.generateCertificateForm.controls.givenName.value as string; }
  get organizationalUnit() { return this.generateCertificateForm.controls.organizationalUnit.value as string; }
  get email() { return this.generateCertificateForm.controls.email.value as string; }

  onGenerateSubmit() {
    const cn = this.commonName;
    const sn = this.surname;
    const givn = this.givenName;
    const orgUnit = this.organizationalUnit;
    const em = this.email;
    const genCert = new GenerateCertificate(cn, sn, givn, orgUnit, em);

    this.userService.generateCertificate(genCert).subscribe(
      response => {
        if (response) {
          this.snackBar.open('Successfully generated new certificate.');
        }
      },
      error => {
        console.log(error);

      }
    )
  }
}
