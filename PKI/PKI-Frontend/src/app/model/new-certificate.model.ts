export class Certificate {

    commonName: string;
    givenName: string;
    surname: string;
    organization: string;
    organizationalUnit: string;
    countryName: string;
    email: string;
    publicKey: string;
    duration: number;
    issuerId: number;
    ca: boolean;
}
