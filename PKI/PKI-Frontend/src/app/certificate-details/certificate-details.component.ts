import { Component, OnInit, Input } from '@angular/core';
import { CertificateService } from '../service/certificate.service';
import { Certificatedto } from '../model/Certificatedto.model';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-certificate-details',
  templateUrl: './certificate-details.component.html',
  styleUrls: ['./certificate-details.component.scss']
})
export class CertificateDetailsComponent implements OnInit {

  constructor(private certificateService: CertificateService, private router: Router, private snackBar: MatSnackBar) { }
  id: number;
  certificate: Certificatedto;
  createCertificate = false;
  displayedColumns: string[] = ['commonName', 'serialNumber', 'organisation', 'organisationUnit', 'country', 'ca', 'validFrom', 'validTo', 'revoked', 'view'];

  ngOnInit() {
    const token = localStorage.getItem('token');
    if (!token) {
      this.router.navigateByUrl('/login')
    }
    if (!this.id) {
      this.certificateService.getRootCertificate().subscribe(
        (response => {
          this.certificate = response;
          console.log(this.certificate);
          this.id = this.certificate.id;
        })
      );
    } else {
      this.certificateService.getCertificate(this.id).subscribe(
        (response => {
          this.certificate = response;
          this.id = this.certificate.id;
        })
      );
    }
  }

  createCertificateButton() {
    this.createCertificate = true;
  }
  revokeCertificate() {
    this.certificateService.revokeCertificate(this.certificate.id).subscribe(
      (response => {
        console.log(response);
        this.certificate.revoked = response.revoked;
        console.log('Certificate with id: ' + this.certificate.id + ' has been revoked.');
      }),
      (error => {
        this.snackBar.open('You do not have authority to revoke: ' + this.certificate.issuerName + ' certificate.');
      })
    );
  }
  viewCertificate(id: number) {
    this.id = id;
    this.certificateService.getCertificate(this.id).subscribe(
      (response => {
        this.certificate = response;
        this.id = this.certificate.id;
      })
    );
  }
  created() {
    this.createCertificate = false;
    if (!this.certificate) {
      this.certificateService.getRootCertificate().subscribe(
        (response => {
          this.certificate = response;
          console.log(this.certificate);
          this.id = this.certificate.id;
        }),
        (error => {
          this.snackBar.open('You do not have authority add this certificate.');
        })
      );
    } else {
      this.certificateService.getCertificate(this.id).subscribe(
        (response => {
          this.certificate = response;
          this.id = this.certificate.id;
        }),
        (error => {
          this.snackBar.open('You do not have authority add this certificate.');
        })
      );
    }
  }
  back() {
    this.id = this.certificate.issuerId;
    this.certificateService.getCertificate(this.id).subscribe(
      (response => {
        this.certificate = response;
        this.id = this.certificate.id;
      })
    );
  }
  notRoot() {
    for (let i = 0; i < this.certificate.childCertificates.length; i++) {
      let cert = this.certificate.childCertificates[i];
      if (cert.id === this.certificate.id) {
        return false;
      }
    }
    return true;
  }
  logOut() {
    localStorage.setItem('token', '');
    this.router.navigateByUrl('/login');
  }
}
