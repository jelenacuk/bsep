package com.pki.pki.converter;

import java.util.Date;
import java.util.Calendar;

import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;

import com.pki.pki.dto.NewCertificateDTO;
import com.pki.pki.model.SubjectData;

public class NewCertificateDTOConverter {

	private static X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE);

	public static SubjectData convertFromDTO(NewCertificateDTO dto){

		SubjectData subjectData = new SubjectData();

		builder.addRDN(BCStyle.CN, dto.getCommonName());
		builder.addRDN(BCStyle.SURNAME, dto.getSurname());
		builder.addRDN(BCStyle.GIVENNAME, dto.getGivenName());
		builder.addRDN(BCStyle.O, dto.getOrganization());
		builder.addRDN(BCStyle.OU, dto.getOrganizationalUnit());
		builder.addRDN(BCStyle.C, dto.getCountryName());
		builder.addRDN(BCStyle.E, dto.getEmail());
		subjectData.setX500Name(builder.build());
		subjectData.setSerialNumber("0");
		
		Date startDate = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(startDate);
		c.add(Calendar.YEAR, dto.getDuration());
		Date endDate =  c.getTime();
				
		subjectData.setStartDate(startDate);
		subjectData.setEndDate(endDate);
		
		return subjectData;
	}
}
