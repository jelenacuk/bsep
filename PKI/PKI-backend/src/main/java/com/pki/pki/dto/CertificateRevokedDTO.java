package com.pki.pki.dto;

public class CertificateRevokedDTO {
	private Boolean revoked;

	public CertificateRevokedDTO(Boolean revoked) {
		super();
		this.revoked = revoked;
	} 

	public CertificateRevokedDTO() {
		super();
	}

	public Boolean getRevoked() {
		return revoked;
	}

	public void setRevoked(Boolean revoked) {
		this.revoked = revoked;
	}
	

}
