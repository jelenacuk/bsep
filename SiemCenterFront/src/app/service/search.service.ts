import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SearchDto } from '../model/SearchDto';
import { Observable } from 'rxjs';
import { Log } from '../model/Log';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }
  path = 'https://localhost:8444';

  search(searchData: SearchDto): Observable<Array<Log>> {
    return this.http.post<Array<Log>>(this.path + '/api/search', searchData);
  }
  searchRegex(searchData: SearchDto): Observable<Array<Log>> {
    return this.http.post<Array<Log>>(this.path + '/api/search/regex', searchData);
  }
}
