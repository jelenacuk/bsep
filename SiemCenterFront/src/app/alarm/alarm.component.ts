import { Component, OnInit, ViewChild } from '@angular/core';
import { Alarm } from '../model/Alarm';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { ReportService } from '../service/reports.service';

@Component({
  selector: 'app-alarm',
  templateUrl: './alarm.component.html',
  styleUrls: ['./alarm.component.scss']
})
export class AlarmComponent implements OnInit {

  constructor(private service: ReportService) { }
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  alarms = new MatTableDataSource<Alarm>();
  displayedColumns: string[] = ['message', 'creationDate', 'appName', 'service', 'user', 'computerName'];
  pageIndex: number = 1;
  length: number = 0;
  ngOnInit() {
    this.callData(this);
    let timerId = setInterval(this.callData, 2000,this);
  }
  callData(that:any) {
    that.service.getAlarms().subscribe(
      response => {
        that.alarms.data = response;
        that.length = that.alarms.data.length;
        that.alarms.paginator = that.paginator;
      }
    );
  }






}
