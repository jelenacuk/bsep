import { Injectable } from '@angular/core';
import { LogIn } from '../model/logIn';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LoginJwt } from '../model/LoginJwt.model';
import { Constants } from './constants';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private constants: Constants) { }



  public login(loginData: LogIn): Observable<LoginJwt> {
    return this.http.post<LoginJwt>(this.constants.path + '/auth/login', loginData);
  }
}
