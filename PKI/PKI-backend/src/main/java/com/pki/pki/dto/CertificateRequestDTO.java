package com.pki.pki.dto;


public class CertificateRequestDTO {

	private String commonName;
	private String surname;
	private String givenName;
	private String organizationalUnit;
	private String organization;
	private String countryName;
	private String email;
	private String alias;
	private String encryptedAlias;
	
	public CertificateRequestDTO() {
	}

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getOrganizationalUnit() {
		return organizationalUnit;
	}

	public void setOrganizationalUnit(String organizationalUnit) {
		this.organizationalUnit = organizationalUnit;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getEncryptedAlias() {
		return encryptedAlias;
	}

	public void setEncryptedAlias(String encryptedAlias) {
		this.encryptedAlias = encryptedAlias;
	}
	
	
}
