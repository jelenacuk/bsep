import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Certificate } from 'src/app/model/new-certificate.model';
import { CertificateService } from 'src/app/service/certificate.service';


@Component({
  selector: 'app-create-certificate',
  templateUrl: './create-certificate.component.html',
  styleUrls: ['./create-certificate.component.scss']
})
export class CreateCertificateComponent implements OnInit {

  certificateForm: FormGroup;
  @Input() issuerId: number = null;
  @Output() createdEvent = new EventEmitter();
  issuerData: any;
  constructor(private formBuilder: FormBuilder, private certificateService: CertificateService) { }

  ngOnInit(): void {
    if (this.issuerId) {
      this.getIssuerData();
    }

    this.certificateForm = this.formBuilder.group({
      commonName: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.pattern('^[A-Za-z][A-Za-z0-9\'\-\.]+([\ A-Za-z][A-Za-z0-9\'\-\.]+)*')
      ]],
      givenName: ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.pattern('^[A-Za-z][A-Za-z\'\-]+([\ A-Za-z][A-Za-z\'\-]+)*')
      ]],
      surname: ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.pattern('^[A-Za-z][A-Za-z\'\-]+([\ A-Za-z][A-Za-z\'\-]+)*')
      ]],
      organization: ['', [
        Validators.required,
        Validators.pattern('^[A-Za-z][A-Za-z0-9\'\-]+([\ A-Za-z][A-Za-z0-9\'\-]+)*')
      ]],
      organizationUnit: ['', [
        Validators.required,
        Validators.pattern('^[A-Za-z][A-Za-z0-9\'\-]+([\ A-Za-z][A-Za-z0-9\'\-]+)*')
      ]],
      country: ['', [
        Validators.required,
        Validators.pattern('^[A-Za-z][A-Za-z]+([\ A-Za-z][A-Za-z]+)*')
      ]],
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      duration: ['', [
        Validators.required
      ]],
      isCa: ['', [
        Validators.required
      ]],
    });
    if (!this.issuerId) {
      this.certificateForm.controls.isCa.setValue(true);
    }
  }

  get commonName() { return this.certificateForm.controls.commonName.value as string; }
  get givenName() { return this.certificateForm.controls.givenName.value as string; }
  get surname() { return this.certificateForm.controls.surname.value as string; }
  get organization() { return this.certificateForm.controls.organization.value as string; }
  get organizationUnit() { return this.certificateForm.controls.organizationUnit.value as string; }
  get country() { return this.certificateForm.controls.country.value as string; }
  get email() { return this.certificateForm.controls.email.value as string; }
  get isCa() { return this.certificateForm.controls.isCa.value as boolean; }
  get duration() { return this.certificateForm.controls.duration.value as number; }

  generateCertificateDTO(): Certificate {
    const certificateDTO = new Certificate();
    certificateDTO.commonName = this.commonName;
    certificateDTO.givenName = this.givenName;
    certificateDTO.surname = this.surname;
    certificateDTO.organization = this.organization;
    certificateDTO.organizationalUnit = this.organizationUnit;
    certificateDTO.countryName = this.country;
    certificateDTO.email = this.email;
    certificateDTO.ca = false;
    if (this.certificateForm.controls.isCa.value === 'true') {
      certificateDTO.ca = true;
    }
    certificateDTO.duration = this.duration;
    certificateDTO.issuerId = this.issuerId;
    return certificateDTO;
  }



  onCreateCertificate() {

    const certificateDTO = this.generateCertificateDTO();
    if (!this.issuerId) {
      this.certificateService.createSelfSignedCertificate(certificateDTO).subscribe(
        (response => {
          if (response != null) {
            alert('Success!');
            this.createdEvent.emit('created');
          }
        }),
        (error => {
          alert(error.error.message);
        }));
    } else {
      this.certificateService.createIssuedCertificate(certificateDTO).subscribe(
        (response => {
          if (response === true) {
            alert('Success!');
            this.createdEvent.emit('created');
          } else {
            alert('Validation failed!');
          }
        }),
        (error => {
          alert(error.error.message);
        }));
    }
  }

  getIssuerData() {

    this.certificateService.getIssuerData(this.issuerId).subscribe(
      (response => {
        if (response) {
          this.issuerData = response;
        }
      }),
      (error => {
        console.log(JSON.stringify(error));
        alert(error.error.message);
      }));
  }
  back() {
    this.createdEvent.emit();
  }

}
