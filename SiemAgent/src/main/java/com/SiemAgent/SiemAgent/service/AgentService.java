package com.SiemAgent.SiemAgent.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sun.jna.platform.win32.WinNT;
import com.SiemAgent.SiemAgent.model.Log;
import com.sun.jna.platform.win32.Advapi32Util.EventLogIterator;
import com.sun.jna.platform.win32.Advapi32Util.EventLogRecord;

@Service
public class AgentService {
	@Value("#{'${custom.paths}'.split(',')}")
	private ArrayList<String> customPaths;

	@Value("#{'${custom.paths.names}'.split(',')}")
	private ArrayList<String> customPathNames;

	@Value("#{'${windows.system.logs.paths}'.split(',')}")
	private ArrayList<String> windowsSystemLogPaths;

	private final String agentLogPath = "C:/TEMP/agent_logs/";
	// private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final String centerPath = "https://localhost:8444/api/center/save";
	RestTemplate restTemplate;

	public AgentService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@PostConstruct
	public void logListener() throws IOException, InterruptedException, IllegalAccessException {
		System.out.println("Here!");

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					readCustomLogs(restTemplate);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();

		// Reads newest Windows system logs
		for (String winSysLogPath : windowsSystemLogPaths) {

			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						readSystemLogs(winSysLogPath, restTemplate);
					} catch (InterruptedException | UnknownHostException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}).start();

		}

	}

	private void readCustomLogs(RestTemplate restTemplate) throws IOException, InterruptedException {
		while (true) {

			int counter = 0;

			for (String path : customPaths) {
				LocalDate now = LocalDate.now();
				BufferedReader reader = new BufferedReader(new FileReader(path));
				FileWriter logWriter = new FileWriter(
						agentLogPath + customPathNames.get(counter) + "_" + now.toString() + ".txt", true);

				String thisLine;
				while ((thisLine = reader.readLine()) != null) {
					logWriter.write(thisLine + "\n");
					restTemplate.postForEntity(centerPath, new Log(thisLine), String.class);
				}

				logWriter.close();
				reader.close();
				counter++;

				// Used for cleaning log file.
				FileWriter fw = new FileWriter(path, false);
				fw.close();
			}

			Thread.sleep(5000);
		}
	}

	/*
	 * Reads Windows system logs using JNA library. path can be System, Application,
	 * Security, or any other Windows system logs path.
	 */
	private void readSystemLogs(String path, RestTemplate restTemplate)
			throws InterruptedException, UnknownHostException {

		EventLogIterator iter = new EventLogIterator(path);

		int lastRecord = 0;
		String thisLine;

		// Finds last record (Log).
		while (iter.hasNext()) {
			EventLogRecord record = iter.next();

			if (!iter.hasNext()) {
				lastRecord = record.getRecordNumber();
			}
		}

		// Reads records from newest to oldest.
		iter = new EventLogIterator(null, path, WinNT.EVENTLOG_BACKWARDS_READ);

		boolean first = true;
		int newLastRecord = 0;

		while (true) {
			while (iter.hasNext()) {
				EventLogRecord record = iter.next();

				// Saves newest record number;
				if (first) {
					newLastRecord = record.getRecordNumber();
					first = false;
				}

				// Resets iterator if last record was reached.
				if (record.getRecordNumber() == lastRecord) {
					lastRecord = newLastRecord;
					iter.close();
					
					break;
				}

				String dateAndTime = new java.text.SimpleDateFormat("MM/dd/YYYY HH:mm:ss").format(new Date());

				ByteBuffer strings = record.getRecord().getPointer().getByteBuffer(
						record.getRecord().StringOffset.longValue(),
						record.getRecord().DataOffset.intValue() - record.getRecord().StringOffset.intValue());
				CharBuffer stringsBuf = strings.asCharBuffer();
				String eventDesc = stringsBuf.toString();

				InetAddress ip = InetAddress.getLocalHost();

				thisLine = record.getInstanceId() + " " + record.getType() + " " + dateAndTime + " " + path + " "
						+ record.getSource() + " " + ip.getHostName() + " " + System.getProperty("user.name") + " "
						+ "Windows" + " " + eventDesc;

				restTemplate.postForEntity(centerPath, new Log(thisLine), String.class);

			}
			Thread.sleep(5000);
			iter = new EventLogIterator(null, path, WinNT.EVENTLOG_BACKWARDS_READ);
			first = true;
		}
	}
}
