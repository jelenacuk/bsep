package com.pki.pki.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.pki.pki.converter.NewCertificateDTOConverter;
import com.pki.pki.dto.CertificateRequestDTO;
import com.pki.pki.dto.IssuerDataDTO;
import com.pki.pki.dto.NewCertificateDTO;
import com.pki.pki.model.Certificate;
import com.pki.pki.model.IssuerData;
import com.pki.pki.model.SubjectData;
import com.pki.pki.repository.CertificateRepository;

@Service
public class CertificateService {

	private final CertificateRepository repository;
	private KeyStoreService keyStoreService;

	private final String path = "./src/main/resources/keystore/";
	@Value("${keystore.filename}")
	private String keyStoreFilename;
	@Value("${keystore.password}")
	private String keyStorePassword;
	@Value("${keystore.keyPassword}")
	private String keyPassword;

	public CertificateService(CertificateRepository repository, KeyStoreService keyStoreService) {

		Security.addProvider(new BouncyCastleProvider());
		this.repository = repository;
		this.keyStoreService = keyStoreService;
	}

	public X509Certificate generateCertificate(SubjectData subjectData, IssuerData issuerData) {
		try {

			JcaContentSignerBuilder builder = new JcaContentSignerBuilder("SHA256WithRSAEncryption");
			builder = builder.setProvider("BC");

			ContentSigner contentSigner = builder.build(issuerData.getPrivateKey());

			X509v3CertificateBuilder certGen = new JcaX509v3CertificateBuilder(issuerData.getX500name(),
					new BigInteger(subjectData.getSerialNumber()), subjectData.getStartDate(), subjectData.getEndDate(),
					subjectData.getX500Name(), subjectData.getPublicKey());
			// Generise se sertifikat
			X509CertificateHolder certHolder = certGen.build(contentSigner);
			JcaX509CertificateConverter certConverter = new JcaX509CertificateConverter();
			certConverter = certConverter.setProvider("BC");
			return certConverter.getCertificate(certHolder);

		} catch (CertificateEncodingException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (OperatorCreationException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		}
		return null;
	}

	public KeyPair generateKeyPair() {
		try {
			// istraziti opcije za algoritme
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
			keyGen.initialize(2048, random);
			return keyGen.generateKeyPair();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void revokeCertificate(Long id) {
		// repository.deleteById(id);
		Certificate c = repository.findById(id).get();
		c.setRevoked(true);
		repository.save(c);
	}

	public boolean generateSelfSignedCertificate(NewCertificateDTO subjectDTO) {

		SubjectData subjectData = NewCertificateDTOConverter.convertFromDTO(subjectDTO);
		KeyPair keyPair = generateKeyPair();
		subjectData.setPublicKey(keyPair.getPublic());
		IssuerData issuerData = new IssuerData(subjectData.getX500Name(), keyPair.getPrivate());

		// Create certificate
		X509Certificate certificate = generateCertificate(subjectData, issuerData);

		// Saving some certificate data into database
		Certificate certEntitity = new Certificate(subjectDTO.getCommonName(), "0", "SHA256WithRSAEncryption", null,
				true, subjectData.getStartDate(), subjectData.getEndDate(), 0, subjectDTO.getOrganization(),
				subjectDTO.getOrganizationalUnit(), subjectDTO.getCountryName());
		certEntitity.setIssuer(certEntitity);
		certEntitity = save(certEntitity);

		// Save new certificate to common keystore
		saveCertToCommonKeystore(certEntitity.getOrganisationUnit(), keyPassword, keyPair.getPrivate(), certificate);

		// Create special keystore for root certificate
		saveCertificateToSeperateKeyStore(certEntitity, certEntitity, keyPair.getPrivate(), certificate);
		return true;
	}

	public boolean generateIssuedCertificate(NewCertificateDTO certDTO) {

		// Validation
		Certificate isssuerCert = findById(certDTO.getIssuerId());
		String issuerAlias = isssuerCert.getOrganisationUnit();
		if (!validate(issuerAlias, isssuerCert)) {
			return false;
		}
		// Generate IssuerData and SubjectData
		SubjectData subjectData = NewCertificateDTOConverter.convertFromDTO(certDTO);
		KeyPair keyPair = generateKeyPair();
		subjectData.setPublicKey(keyPair.getPublic());
		PrivateKey subjectPrivateKey = keyPair.getPrivate();
		IssuerData issuerData = keyStoreService.readIssuerFromStore(keyStoreFilename, isssuerCert.getOrganisationUnit(),
				keyStorePassword.toCharArray(), keyPassword.toCharArray());

		// Saving some certificate data into database
		Integer issuerCertCnt = isssuerCert.getIssuedCertCounter() + 1;
		isssuerCert.setIssuedCertCounter(issuerCertCnt);
		Certificate newCertificate = new Certificate(certDTO.getCommonName(), issuerCertCnt.toString(),
				"SHA256WithRSAEncryption", null, certDTO.isCA(), subjectData.getStartDate(), subjectData.getEndDate(),
				0, certDTO.getOrganization(), certDTO.getOrganizationalUnit(), certDTO.getCountryName());
		isssuerCert = save(isssuerCert);
		newCertificate.setIssuer(isssuerCert);
		newCertificate = save(newCertificate);

		// Create certificate
		subjectData.setSerialNumber(issuerCertCnt.toString());
		X509Certificate certificate = generateCertificate(subjectData, issuerData);
		// Store to common keystore
		saveCertToCommonKeystore(newCertificate.getOrganisationUnit(), keyPassword, subjectPrivateKey, certificate);
		// create new keystore
		saveCertificateToSeperateKeyStore(isssuerCert, newCertificate, subjectPrivateKey, certificate);
		addToTruststore(isssuerCert, newCertificate, certificate);
		return true;
	}

	public void addToTruststore(Certificate issuerDB, Certificate subjectDB, X509Certificate subjectCert) {

		String issuerFilename = "./src/main/resources/keystore/" + issuerDB.getIssuer().getOrganisationUnit() + "_to_"
				+ issuerDB.getOrganisationUnit() + ".jks";
		String subjectFilename = "./src/main/resources/keystore/" + subjectDB.getIssuer().getOrganisationUnit() + "_to_"
				+ subjectDB.getOrganisationUnit() + ".jks";
		X509Certificate issuerCert = null;

		try {
			KeyStore issuerTrustStore = KeyStore.getInstance("JKS", "SUN");
			issuerTrustStore.load(new FileInputStream(issuerFilename), keyStorePassword.toCharArray());
			issuerTrustStore.setCertificateEntry(subjectDB.getOrganisationUnit(), subjectCert);
			issuerTrustStore.store(new FileOutputStream(issuerFilename), keyStorePassword.toCharArray());
			issuerCert = (X509Certificate) issuerTrustStore.getCertificate(issuerDB.getOrganisationUnit().toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			KeyStore subjectTrustStore = KeyStore.getInstance("JKS", "SUN");
			subjectTrustStore.load(new FileInputStream(subjectFilename), keyStorePassword.toCharArray());
			subjectTrustStore.setCertificateEntry(issuerDB.getOrganisationUnit(), issuerCert);
			subjectTrustStore.store(new FileOutputStream(subjectFilename), keyStorePassword.toCharArray());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public IssuerDataDTO getIssuerData(Long issuerAlias) {
		Certificate issuer = findById(issuerAlias);
		IssuerDataDTO data = new IssuerDataDTO(issuer.getOrganisation(), issuer.getOrganisationUnit(),
				issuer.getCountry(), issuer.getCommonName(), issuer.getValidFrom(), issuer.getValidTo());
		return data;
	}

	public String certificateRequest(CertificateRequestDTO dto) {
		byte[] bytes = Base64.getDecoder().decode(dto.getEncryptedAlias());
		keyStoreService.loadKeyStore(keyStoreFilename, keyStorePassword.toCharArray());
		X509Certificate siemCertificate = (X509Certificate) keyStoreService.readCertificate(keyStoreFilename,
				keyStorePassword, dto.getAlias());
		PublicKey publicKey = siemCertificate.getPublicKey();
		byte[] returnValue = null;
		
		boolean encrypted = false;
		try {
			Cipher decrypt = Cipher.getInstance("RSA");
			decrypt.init(Cipher.DECRYPT_MODE, publicKey);
			byte[] decryptedMessage = decrypt.doFinal(bytes);
			String decryptedMessageString = new String(decryptedMessage);
			if (decryptedMessageString.equals(dto.getAlias())) {
				encrypted = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (encrypted) {
			Certificate siemEntity = repository.findOneByOrganisationUnit(dto.getAlias());
			NewCertificateDTO newCertDTO = new NewCertificateDTO(siemEntity.getId(), siemEntity.getOrganisation(), siemEntity.getCountry(), dto);
			boolean created = this.generateIssuedCertificate(newCertDTO);
			if (created) {
				String fileName = siemEntity.getOrganisationUnit() + "_to_" + dto.getOrganizationalUnit();
				
				//InputStream in = getClass().getResourceAsStream("keystore/" + fileName + ".jks");
				try {
					ClassPathResource classPathResource = new ClassPathResource("keystore/" + fileName+ ".jks");
					ClassPathResource classPathResource2 = new ClassPathResource("keystore/" + "PKI_to_PKI"+ ".jks");
					InputStream in = new FileInputStream("./src/main/resources/keystore/"+fileName+".jks");
					returnValue = IOUtils.toByteArray(in);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return Base64.getEncoder().encodeToString(returnValue);
	}

	private boolean validate(String alias, Certificate issuerCert) {
		// isCA
		if (!issuerCert.isCA()) {
			return false;
		}
		// Dates
		else if (issuerCert.getValidTo().before(new Date()) || issuerCert.getValidFrom().after(new Date())) {

			return false;
		}
		// sign and revocation
		else if (!validateChain(alias, issuerCert)) {
			System.out.println("LANAC");
			return false;
		}
		return true;
	}

	private boolean validateChain(String alias, Certificate cert) {
		Certificate issuerCert = findById(cert.getIssuer().getId());
		boolean signed = true;
		try {
			java.security.cert.Certificate cert_ = keyStoreService.readCertificate(keyStoreFilename, keyStorePassword,
					alias);
			java.security.cert.Certificate issuerCert_ = keyStoreService.readCertificate(keyStoreFilename,
					keyStorePassword, cert.getIssuer().getOrganisationUnit());
			cert_.verify(issuerCert_.getPublicKey());
		} catch (Exception e) {
			signed = false;
		}
		boolean ok = !cert.getRevoked() && signed;
		if (cert.getId() == cert.getIssuer().getId()) {
			return ok;
		}
		return ok && validateChain(issuerCert.getOrganisationUnit(), issuerCert);
	}

	private void saveCertToCommonKeystore(String alias, String keyPass, PrivateKey privateKey,
			X509Certificate certificate) {

		keyStoreService.loadKeyStore(keyStoreFilename, keyStorePassword.toCharArray());
		keyStoreService.write(alias, privateKey, keyPass.toCharArray(), certificate);
		keyStoreService.saveKeyStore(keyStoreFilename, keyStorePassword.toCharArray());
	}

	private void saveCertificateToSeperateKeyStore(Certificate issuer, Certificate subject, PrivateKey privateKey,
			X509Certificate certificate) {
		String filename = path + issuer.getOrganisationUnit() + "_to_" + subject.getOrganisationUnit() + ".jks";
		keyStoreService.loadKeyStore(null, keyStorePassword.toCharArray());
		keyStoreService.write(subject.getOrganisationUnit(), privateKey, keyPassword.toCharArray(), certificate);
		keyStoreService.saveKeyStore(filename, keyStorePassword.toCharArray());
	}

	public List<Certificate> findRevoked() {
		return repository.findByRevokedTrue();
	}

	public Certificate save(Certificate certificate) {
		return repository.save(certificate);
	}

	public List<Certificate> findRoots() {
		return repository.findRoots();
	}

	public Certificate findById(Long id) {
		return repository.findById(id).get();
	}

	public List<Certificate> findChildrenCertificates(Certificate issuerName) {
		return repository.findByIssuer(issuerName);

	}
}
