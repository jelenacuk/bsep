import { TestBed } from '@angular/core/testing';

import { AlarmsGuard } from './alarms.guard';

describe('AlarmsGuard', () => {
  let guard: AlarmsGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AlarmsGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
