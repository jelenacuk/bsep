package com.siem.SIEMCenter.dto;

public class LogDTO {
	
	private String text;

	public LogDTO() {
		super();
	}
	

	public LogDTO(String text) {
		super();
		this.text = text;
	}


	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	

}
