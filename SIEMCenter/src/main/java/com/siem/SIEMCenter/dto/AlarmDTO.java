package com.siem.SIEMCenter.dto;

import java.util.Date;

import com.siem.SIEMCenter.model.Alarm;

public class AlarmDTO {
	private String id;
	private String message;
	private String appName;
	private String service;
	private String computerName;
	private String user;
	private Date creationDate;
	public AlarmDTO() {
		
	}


	public AlarmDTO(Alarm alarm) {
		this.id = alarm.getId();
		this.message = alarm.getMessage();
		this.appName = alarm.getAppName();
		this.service = alarm.getService();
		this.computerName = alarm.getComputerName();
		this.user = alarm.getUser();
		this.creationDate = alarm.getCreationDate();
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getAppName() {
		return appName;
	}


	public void setAppName(String appName) {
		this.appName = appName;
	}


	public String getService() {
		return service;
	}


	public void setService(String service) {
		this.service = service;
	}


	public String getComputerName() {
		return computerName;
	}


	public void setComputerName(String computerName) {
		this.computerName = computerName;
	}


	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	public Date getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
}
