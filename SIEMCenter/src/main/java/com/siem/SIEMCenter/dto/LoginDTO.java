package com.siem.SIEMCenter.dto;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

public class LoginDTO {

	@NotNull
	@Size(min = 5)
	@Pattern(regexp = "[A-Za-z0-9]+")
	private String username;
	@NotNull
	@Size(min = 5)
	@Pattern(regexp = "[A-Za-z0-9]+")
	private String password;

	public LoginDTO() {
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
