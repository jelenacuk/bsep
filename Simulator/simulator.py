import time
import random
import threading
import json
import log_generator
from os import path as path_lib
from pathlib import Path

shouldRun = [False,False,False]
thread_path = ["","",""]
def generateLog():
    return "log"

def runApp(path,mode,name,computer_name,user,threadNo, system):
    while shouldRun[threadNo]:
        log_generator.generate_log(path, mode, name, computer_name, user, system)
        time.sleep(random.randint(0, 5))

def print_thread_status(thread_number):
    global shouldRun
    if (shouldRun[thread_number]):
        print("Thread "+ str(thread_number)+" is Active")
    else:
        print("Thread " + str(thread_number) + " is NOT Active")
def change_flag():
    global shouldRun
    global thread_path
    while True:
        print_thread_status(0)
        print_thread_status(1)
        print_thread_status(2)
        a = input("Enter thread num:")
        if(a == "0"):
            shouldRun[0] = not shouldRun[0]
            if shouldRun[0]:
                mode = input("Enter mode")
                x = threading.Thread(target=runApp, args=(thread_path[0], mode, "App1", "Comp3", "Mile",0, "Windows"))
                x.start()
        elif(a == "1"):
            shouldRun[1] = not shouldRun[1]
            if shouldRun[1]:
                mode = input("Enter mode")
                x = threading.Thread(target=runApp, args=(thread_path[1], mode, "App2", "Comp2", "Jeca",1, "Windows"))
                x.start()
        elif (a == "2"):
            shouldRun[2] = not shouldRun[2]
            if shouldRun[2]:
                mode = input("Enter mode")
                x = threading.Thread(target=runApp, args=(thread_path[2], mode, "App3", "Comp1", "Nikola", 2, "Linux"))
                x.start()

def main():
    global thread1Path
    with open('configuration.json') as json_file:
        data = json.load(json_file)
        lista = data["threads"]
        num = 0
        for i in lista:
            path = i["path"]
            path_splitted = path.split('/')
            path_folder = ""
            for ind in range(0,len(path_splitted)):
                if ind == 0:
                    path_folder += path_splitted[ind]
                elif ind < len(path_splitted)-1:
                    path_folder += '/'+path_splitted[ind]
            print(path_folder)
            if not path_lib.exists(path_folder):
                Path(path_folder).mkdir(parents=True, exist_ok=True)
            mode = i["mode"]
            thread_path[num] = path
            x = threading.Thread(target=runApp, args=(path, mode,"App1","Comp1","Pera",num, "Windows"))
            shouldRun[num] = True
            x.start()
            num +=1
    change_flag()

main()