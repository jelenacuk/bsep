import { TestBed } from '@angular/core/testing';

import { RequestCertificateGuard } from './request-certificate.guard';

describe('RequestCertificateGuard', () => {
  let guard: RequestCertificateGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(RequestCertificateGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
